package com.server.Manager;

import com.gpudb.*;
import java.util.Optional;

public class GpuDbInstanceManager 
{

    private Optional<GPUdb> gpuDb = Optional.empty();
    private String gpuDbHost;
    private String gpuDbPort;

    private static Optional<GpuDbInstanceManager> instance = Optional.empty();

    private GpuDbInstanceManager()
    {

    }

    public String getGpuDbHost() 
    {
        return gpuDbHost;
    }

    public void setGpuDbHost(String gpuDbHost) 
    {
        this.gpuDbHost = gpuDbHost;
    }

    public String getGpuDbPort() 
    {
        return gpuDbPort;
    }

    public void setGpuDbPort(String gpuDbPort) 
    {
        this.gpuDbPort = gpuDbPort;
    }

    /*
        Enforce Singleton instance
     */
    public static Optional<GpuDbInstanceManager> getInstance(final String host, final String port, final String user, final String password)
    {
        if( !instance.isPresent())
        {
            instance = Optional.of(new GpuDbInstanceManager());
            GpuDbInstanceManager lGpuDbInstanceMgr = instance.get();
            lGpuDbInstanceMgr.setGpuDbHost(host);
            lGpuDbInstanceMgr.setGpuDbPort(port);

            try 
            {
               // lGpuDbInstanceMgr.gpuDb = Optional.of(new GPUdb(host + ":" + port));
               // lGpuDbInstanceMgr.gpuDb = Optional.of(new GPUdb(host + "/" + port));
                lGpuDbInstanceMgr.gpuDb = Optional.of(new GPUdb(host + ":" + port,  new  GPUdbBase.Options().setUsername(user).setPassword(password)));
            } 
            catch (GPUdbException e) 
            {
                e.printStackTrace();
                lGpuDbInstanceMgr.gpuDb = Optional.empty();
            }
        }
        return instance;
    }

    public Optional<GPUdb> getDbInstance()
    {
        return gpuDb;
    }
}