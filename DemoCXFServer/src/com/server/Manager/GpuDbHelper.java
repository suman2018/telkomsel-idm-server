package com.server.Manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.gpudb.GPUdb;
import com.gpudb.GPUdbException;
import com.gpudb.protocol.AlterUserResponse;
import com.gpudb.protocol.CreateUserExternalResponse;
import com.gpudb.protocol.CreateUserInternalResponse;
import com.gpudb.protocol.DeleteUserRequest;
import com.gpudb.protocol.DeleteUserResponse;
import com.gpudb.protocol.ShowSecurityResponse;
import com.server.com.Response;

public class GpuDbHelper 
{
	private static String host = "http://localhost";
	// private static String host ="http://10.54.168.118";
	private static String port = "9191";
	private static String user = "admin";
	private static String password = "Kinetica1!";
	private static GpuDbHelper gpuDbHelper = null;

	public static GpuDbHelper getInstance() 
	{
		if (gpuDbHelper == null) 
		{
			gpuDbHelper = new GpuDbHelper();
		}
		return gpuDbHelper;
	}

	public Response createUserExternal(String name) 
	{
		GpuDbInstanceManager gpuDbInstanceManager = GpuDbInstanceManager.getInstance(host, port, user, password).get();
		GPUdb gpudb = gpuDbInstanceManager.getDbInstance().get();
		CreateUserExternalResponse createUserExternalResponse = null;
		Response response = new Response();
		try 
		{
			createUserExternalResponse = gpudb.createUserExternal(name, null);
			response.setResultID("success");
			response.setResultDesc(createUserExternalResponse.getName() + " created as External User");
		} 
		catch (GPUdbException e) 
		{
			response.setResultID("fail");
			response.setResultDesc(e.toString());
		}
		return response;
	}

	public Response createUserInternal(String name, String pass) 
	{
		GpuDbInstanceManager gpuDbInstanceManager = GpuDbInstanceManager.getInstance(host, port, user, password).get();
		GPUdb gpudb = gpuDbInstanceManager.getDbInstance().get();
		CreateUserInternalResponse createUserInternalResponse = null;
		Response response = new Response();
		try 
		{
			createUserInternalResponse = gpudb.createUserInternal(name, pass, null);
			response.setResultID("success");
			response.setResultDesc(createUserInternalResponse.getName() + " created as Internal User");
		} 
		catch (GPUdbException e) 
		{
			response.setResultID("fail");
			response.setResultDesc(e.toString());
		}
		return response;
	}

	public Response alterUser(String name, String action, String value, Map<String, String> options) 
	{
		GpuDbInstanceManager gpuDbInstanceManager = GpuDbInstanceManager.getInstance(host, port, user, password).get();
		GPUdb gpudb = gpuDbInstanceManager.getDbInstance().get();
		// AlterUserRequest alterUser = new AlterUserRequest(name, action, value, options);
		AlterUserResponse alterUserResponse = null;
		Response response = new Response();
		try 
		{
			alterUserResponse = gpudb.alterUser(name, action, value, options);
			response.setResultID("success");
			response.setResultDesc(alterUserResponse.getName() + " altered");
		} 
		catch (GPUdbException e) 
		{
			response.setResultID("fail");
			response.setResultDesc(e.toString());
		}
		return response;
	}
	
	public Response deleteUser(String name, String username, String password, Map<String, String> options) 
	{
		GpuDbInstanceManager gpuDbInstanceManager = GpuDbInstanceManager.getInstance(host, port, username, password).get();
		GPUdb gpudb = gpuDbInstanceManager.getDbInstance().get();
		//DeleteUserRequest deleteUser = new DeleteUserRequest(name, options);
		DeleteUserResponse deleteUserResponse = null;
		Response response = new Response();
		try 
		{
			deleteUserResponse = gpudb.deleteUser(name, options);
			response.setResultID("success");
			response.setResultDesc(deleteUserResponse.getName() + " delete");
		} 
		catch (GPUdbException e) 
		{
			response.setResultID("fail");
			response.setResultDesc(e.toString());
		}
		return response;
	}

	public Response getAllUser(String Name, String Password) 
	{
		GpuDbInstanceManager gpuDbInstanceManager = GpuDbInstanceManager.getInstance(host, port, Name, Password).get();
		GPUdb gpudb = gpuDbInstanceManager.getDbInstance().get();
		Map<String, String> userCollection;
		Response response = new Response();
		List<String> users = new ArrayList<String>();
		ShowSecurityResponse showSecurityResponse = null;
		try 
		{
			showSecurityResponse = gpudb.showSecurity(null, null);
			userCollection = showSecurityResponse.getTypes();
			if (userCollection != null && !userCollection.isEmpty()) 
			{
				for (Map.Entry<String, String> entry : userCollection.entrySet()) 
				{
					if ((entry.getValue().equals("internal_user")) || (entry.getValue().equals("external_user")))
						users.add(entry.getKey());
				}
				response.setResultID("success");
				response.setResultDesc("User List Found");
				response.setUsers(users.toArray(new String[0]));
			}
			else
			{
				response.setResultID("fail");
				response.setResultDesc("Provide Correct User or Check Connection");
			}
		} 
		catch (GPUdbException e) 
		{
			response.setResultID("fail");
			response.setResultDesc(e.toString());
		}
		return response;
	}

	public Response getAllRoles(String Name, String Password) 
	{
		GpuDbInstanceManager gpuDbInstanceManager = GpuDbInstanceManager.getInstance(host, port, Name, Password).get();
		GPUdb gpudb = gpuDbInstanceManager.getDbInstance().get();
		Map<String, String> roleCollection;
		Response response = new Response();
		List<String> roles = new ArrayList<String>();
		ShowSecurityResponse showSecurityResponse = null;
		try 
		{
			showSecurityResponse = gpudb.showSecurity(null, null);
			roleCollection = showSecurityResponse.getTypes();
			if (roleCollection != null && !roleCollection.isEmpty()) 
			{				
				for (Map.Entry<String, String> entry : roleCollection.entrySet()) 
				{
					if (entry.getValue().equals("role"))
						roles.add(entry.getKey());
				}
				response.setResultID("success");
				response.setResultDesc("Role List Found");
				response.setRoles(roles.toArray(new String[0]));
			}
			else
			{
				response.setResultID("fail");
				response.setResultDesc("Provide Correct User or Check Connection");
			}
		} 
		catch (GPUdbException e) 
		{
			response.setResultID("fail");
			response.setResultDesc(e.toString());
		}
		return response;
	}
}
