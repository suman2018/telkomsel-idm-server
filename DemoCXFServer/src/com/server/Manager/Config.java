package com.server.Manager;

import java.io.InputStream;
import java.util.Properties;

public class Config 
{
	Properties configFile;
	InputStream inputStream;
	
	public Config()
	{
		configFile = new java.util.Properties();
		try 
		{
			inputStream = this.getClass().getClassLoader().getResourceAsStream("config.properties");
			if(inputStream != null)
				configFile.load(inputStream);
				
		}
		catch(Exception e)
		{
		    e.printStackTrace();
		}
	}

	public String getProperty(String key)
	{
		String value = this.configFile.getProperty(key);
		return value;
	}
}
