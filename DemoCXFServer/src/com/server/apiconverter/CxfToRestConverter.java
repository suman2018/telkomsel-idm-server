package com.server.apiconverter;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.server.Manager.GpuDbHelper;
import com.server.com.Response;

@WebService(targetNamespace = "http://apiconverter.server.com/", portName = "CxfToRestConverter", serviceName = "CxfToRestConverterService")
public class CxfToRestConverter implements CxfToRestConverterInterface 
{
	static GpuDbHelper _helper = new GpuDbHelper();	
	
	@WebMethod(operationName = "CreateUserExternal", action = "urn:CreateUserExternal")
	public Response createUserExternal(@WebParam(name = "Name") String Name, @WebParam(name = "Password") String Password, @WebParam(name = "EmpId") String EmpId, @WebParam(name = "UserId") String UserId, @WebParam(name = "UserName") String UserName, @WebParam(name = "Role") String Role) 
	{
		Response result = _helper.createUserExternal("@"+UserName);
		return result;
	}
	
	@WebMethod(operationName = "CreateUser", action = "urn:CreateUser")
	public Response createUser(@WebParam(name = "Name") String Name, @WebParam(name = "Password") String Password, @WebParam(name = "EmpId") String EmpId, @WebParam(name = "UserId") String UserId, @WebParam(name = "UserName") String UserName, @WebParam(name = "Role") String Role) 
	{
		Response result = _helper.createUserInternal(UserName, Password);
		return result;
	}	
	
	@WebMethod(operationName = "ModifyUser", action = "urn:ModifyUser")
	public Response modifyUser(@WebParam(name = "Name") String Name, @WebParam(name = "Password") String Password, @WebParam(name = "EmpId") String EmpId, @WebParam(name = "UserId") String UserId, @WebParam(name = "UserName") String UserName, @WebParam(name = "Role") String Role) 
	{
		Response result = _helper.alterUser(UserName, "set_password", Password, null);;
		return result;
	}	
	
	@WebMethod(operationName = "DeleteUser", action = "urn:DeleteUser")
	public Response deleteUser(@WebParam(name = "Name") String Name, @WebParam(name = "Password") String Password, @WebParam(name = "UserName") String UserName) 
	{
		Response result = _helper.deleteUser(Name, UserName, Password, null);;
		return result;
	}	
	
	@WebMethod(operationName = "GetUserDetails", action = "urn:GetUserDetails")
	public Response getUserDetails(@WebParam(name = "Name") String Name, @WebParam(name = "Password") String Password, @WebParam(name = "EmpId") String EmpId) 
	{
		// TODO 
		return null;
	}
	
	@WebMethod(operationName = "GetAllUsers", action = "urn:GetAllUsers")
	public Response getAllUsers(@WebParam(name = "Name") String Name, @WebParam(name = "Password") String Password) 
	{			
		Response userList = _helper.getAllUser(Name, Password);
		return userList;
	}
	
	@WebMethod(operationName = "GetAllRoles", action = "urn:GetAllRoles")
	public Response getAllRoles(@WebParam(name = "Name") String Name, @WebParam(name = "Password") String Password) 
	{
		Response response = _helper.getAllRoles(Name, Password);		
		return response;
	}	
	
	@WebMethod(operationName = "SayHello", action = "urn:SayHello")
	public String sayHello(@WebParam(name = "name") String name)
	{
		return "Hello "+ name;
	}	
}
