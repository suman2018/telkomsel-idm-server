package com.server.apiconverter;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.server.com.Response;


@WebService
public interface CxfToRestConverterInterface 
{
	@WebMethod
	public Response createUserExternal(String Name, String Password, String EmpId, String UserId, String UserName, String Role);
	
	@WebMethod
	public Response createUser(String Name, String Password, String EmpId, String UserId, String UserName, String Role); 
	
	@WebMethod
	public Response modifyUser(String Name, String Password, String EmpId, String UserId, String UserName, String Role);
	
	@WebMethod
	public Response deleteUser(String Name, String Password, String UserName);
	
	@WebMethod
	public Response getUserDetails(String Name, String Password, String EmpId);
	
	@WebMethod
	public Response getAllUsers(String Name, String Password);
	
	@WebMethod
	public Response getAllRoles(String Name, String Password);	
}
