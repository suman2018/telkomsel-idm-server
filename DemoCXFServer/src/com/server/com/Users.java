package com.server.com;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userList"
})

@XmlRootElement(name = "Users")
public class Users
{
	@XmlElement(name = "userList")
	private List<User> userList;
	
	public List<User> getUsers() 
	{
		return userList;
	}

	public void setUsers(List<User> userList) 
	{
		this.userList = userList;
	}	
}
