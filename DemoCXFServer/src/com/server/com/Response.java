package com.server.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Response", propOrder = {
    "ResultID",
    "ResultDesc",
    "Users",
    "Roles"
})

@XmlRootElement(name = "Response")
public class Response 
{	
	private String ResultID;
	private String ResultDesc;
	private String [] Users;
	private String [] Roles;
	
	public String getResultID() {
		return ResultID;
	}
	public void setResultID(String resultID) {
		ResultID = resultID;
	}
	public String getResultDesc() {
		return ResultDesc;
	}
	public void setResultDesc(String resultDesc) {
		ResultDesc = resultDesc;
	}
	public String[] getUsers() {
		return Users;
	}
	public void setUsers(String[] users) {
		Users = users;
	}
	public String [] getRoles() {
		return Roles;
	}
	public void setRoles(String [] roles) {
		Roles = roles;
	}
	
}
